package br.edu.unisep.store.domain.dto;

import lombok.Getter;

@Getter
public class RegisterProductDto {

    public String name;

    public String description;

    public Float price;

    public String brand;

    public Integer status;

}
