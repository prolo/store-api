package br.edu.unisep.store.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ProductDto {

    public final Integer id;

    public final String name;

    public final String description;

    public final Float price;

    public final String brand;

    public final Integer status;

    public final String satus_description;

}
