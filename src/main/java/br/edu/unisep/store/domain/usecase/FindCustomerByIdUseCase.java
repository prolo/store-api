package br.edu.unisep.store.domain.usecase;

import br.edu.unisep.store.data.dao.CustomerDao;
import br.edu.unisep.store.domain.builder.CustomerBuilder;
import br.edu.unisep.store.domain.dto.CustomerDto;

public class FindCustomerByIdUseCase {

    public CustomerDto execute(Integer id) {
        var dao = new CustomerDao();

        if (id == 0) {
            throw new IllegalArgumentException("Informe o id do cliente");
        }
        var customer = dao.findById(id);

        var builder = new CustomerBuilder();
        return builder.from(customer);
    }
}
