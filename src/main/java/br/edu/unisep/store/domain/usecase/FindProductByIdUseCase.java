package br.edu.unisep.store.domain.usecase;

import br.edu.unisep.store.data.dao.ProductDao;
import br.edu.unisep.store.domain.builder.ProductBuilder;
import br.edu.unisep.store.domain.dto.ProductDto;

public class FindProductByIdUseCase {

    public ProductDto execute(Integer id) {
        var dao = new ProductDao();

        if (id == 0) {
            throw new IllegalArgumentException("Informe o id do cliente");
        }
        var product = dao.findById(id);

        var builder = new ProductBuilder();
        return builder.from(product);
    }

}
