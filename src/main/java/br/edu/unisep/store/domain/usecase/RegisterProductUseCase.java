package br.edu.unisep.store.domain.usecase;

import br.edu.unisep.store.data.dao.ProductDao;
import br.edu.unisep.store.domain.builder.ProductBuilder;
import br.edu.unisep.store.domain.dto.RegisterProductDto;
import br.edu.unisep.store.domain.validator.ProductValidator;

public class RegisterProductUseCase {

    public void execute(RegisterProductDto registerProduct) {
        var validator = new ProductValidator();
        validator.validate(registerProduct);

        var builder = new ProductBuilder();
        var product = builder.from(registerProduct);

        var dao = new ProductDao();
        dao.save(product);
    }

}
