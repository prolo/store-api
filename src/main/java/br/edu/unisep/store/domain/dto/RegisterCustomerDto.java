package br.edu.unisep.store.domain.dto;

import lombok.Getter;

import java.time.LocalDate;

@Getter
public class RegisterCustomerDto {

    private String name;
    private String email;

    private LocalDate birthdate;

    private String cpf;
}
