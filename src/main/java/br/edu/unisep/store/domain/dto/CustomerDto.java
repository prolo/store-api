package br.edu.unisep.store.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.LocalDate;

@Getter
@AllArgsConstructor
public class CustomerDto {

    private final Integer id;

    private final String name;

    private final String email;

    private final LocalDate birthdate;

    private final String cpf;

    private final long age;
}
