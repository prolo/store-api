package br.edu.unisep.store.data.dao;

import br.edu.unisep.store.data.entity.Product;
import br.edu.unisep.store.data.hibernate.HibernateSessionFactory;

import java.util.List;

public class ProductDao {

    public List<Product> findAll() {
        var session = HibernateSessionFactory.getSession();

        var query = session.createQuery("from Product", Product.class);
        var result = query.list();

        session.close();

        return result;
    }

    public Product findById(Integer id) {
        var session = HibernateSessionFactory.getSession();

        var query = session.createQuery("from Product where id = :pId", Product.class);
        query.setParameter("pId", id);

        var result = query.uniqueResult();

        session.close();

        return result;
    }

    public void save(Product product) {
        var session = HibernateSessionFactory.getSession();
        var transaction = session.beginTransaction();

        try {
            session.save(product);
            transaction.commit();
        } catch (Exception error) {
            error.printStackTrace();
            transaction.rollback();
        }

        session.close();
    }

    public void delete(Product product) {
        var session = HibernateSessionFactory.getSession();
        var transaction = session.beginTransaction();

        try {
            session.delete(product);
            transaction.commit();
        } catch (Exception error) {
            error.printStackTrace();
            transaction.rollback();
        }

        session.close();
    }

}
